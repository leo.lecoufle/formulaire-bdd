<?php
/**
 * Requète qui affiche tout
 */
const REQ_FULL_LIST="SELECT * FROM t_msg LEFT JOIN t_email ON t_msg.id_msg = t_email.id_Email";
/**
 * Regroupe tous les messages par email

 */
 const REQ_MSG_PER_EMAIL  = "SELECT * FROM t_msg WHERE id_email = :ValEmailID";


/**
 * nombre de message reçu par mois
 */
const REQ_NB_MSG_PER_MONTH ="SELECT COUNT(date_msg) FROM t_msg WHERE MONTH(DATE(date_msg))= :Mois";
/**
 * Etat du message/ traitement du message 
 */
const REQ_ETAT_MAIL_LIST ="SELECT * FROM t_msg WHERE etat = :Etat";


/**
 * Récupérer l'état du message par id
 */
const REQ_ETAT_MAIL_PER_ID ="SELECT etat FROM t_msg WHERE id_email = :ValEmailID";

/**
 * Mise à jour de létat des mails 
 */
const REQ_UPDATE_MAIL_ETAT ="UPDATE t_msg SET etat = :NewEtat WHERE id_email = :ValEmailID";


/**
 * Add a personne
 *
 */
const REQ_PERSON_ADD = "INSERT INTO t_personne (prenom, nom, tel, id_Email) VALUES (:ValPrenom, :ValNom, :ValTel, :ValEmailID)";