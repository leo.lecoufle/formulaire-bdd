<?php
// Pour forcer la déclaration des variables jusqu'à  PHP7 1=true
declare(strict_types=1);

require_once "DbServerParams.php";
require_once "FormRequests.php";


// Paramètre par défaut
function RequestManager(object $RequestToHandle = null, string $InfoMessage = "", string $NoDataMessage = "", array $RequestParams = [], $IsSelectRequest = true)
{    if (is_a($RequestToHandle, "PDOStatement")) {
        // Essai de se connecter à la base de donnée et gestion des erreurs
        try {
            $RequestToHandle->execute($RequestParams);
            $Nbr = $RequestToHandle->rowCount();
            if (0 != $Nbr) {
                print PHP_EOL . $InfoMessage . PHP_EOL . PHP_EOL;
                if ($IsSelectRequest) {
                    // FETCH_ASSOC réponse sous forme de tableau associatif
                    while ($DataLine = $RequestToHandle->fetch(PDO::FETCH_ASSOC)) {
                        print_r($DataLine);
                    }
                }
            } else {
                print $NoDataMessage . PHP_EOL . PHP_EOL;
            }
            // Exception $...: est une variable. Création des types d'erreurs,vous pouvez attraper l'erreur pour l'afficher comme vous voulez plutÃƒÂ´t que d'avoir un fameux et plutÃƒÂ´t laid Warning
        } catch (Exception $ExceptionRaised) {
            print "Une erreur inattendue est survenue !" . PHP_EOL;
            print ("Erreur : " . $ExceptionRaised->getMessage()) . PHP_EOL;
            print "Informations sur l'état de la requète :" . PHP_EOL;
            print_r($RequestToHandle->errorInfo());
        }
    } else {
        print "ERREUR !!! La variable passée en 1er argument de RequestManager() n'est pas une requète de type PDOStatement" . PHP_EOL . PHP_EOL;
    }
}

(object) $MyDB = null;
(bool) $PromptPwd = false;

try {
    $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, DEFAULT_DB_LOGIN, DEFAULT_DB_PWD, array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
    ));
} catch (Exception $ExceptionRaised) {
    print "La connexion n'a pas pu se faire avec les identifiants par défaut." . PHP_EOL . PHP_EOL;
    $PromptPwd = true;
}
if ($PromptPwd) {
    // Demander l'identifiant et le mot de passe
    print("Saisissez le nom du compte pour vous connecter au serveur MySQL : ");
    // fgets(STDIN) pour récupérer en ligne de commande
    (string) $DbServerLogin = trim(fgets(STDIN));
    print("Saisissez le mot de passe : ");
    (string) $DbServerPassword = trim(fgets(STDIN));
    // connexion à la bdd
    try {
        $MyDB = new PDO(DB_DRIVER . ":host=" . DB_HOST . ";dbname=" . DB_NAME, $DbServerLogin, $DbServerPassword);
    } catch (Exception $ExceptionRaised) {
        print "La connexion au serveur MySQL est impossible. Voir le détail ci-dessous." . PHP_EOL;
        print_r($MyDB->errorInfo());
        die("Erreur : " . $ExceptionRaised->getMessage());
    }
}
print PHP_EOL . "La connexion au serveur de bases de données est désormée active." . PHP_EOL . PHP_EOL;
// setAttribute Configure un attribut du gestionnaire de base de données(dans notre cas la gestion des erreurs)
// PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION gestion des erreurs
$MyDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// Préparation des requètes utilisées souvent



print PHP_EOL . "Préparation des requètes." . PHP_EOL . PHP_EOL;
// prepare =>Pépare une requète à  l'exécution et retourne un objet !important

$MyReqFullList = $MyDB->prepare(REQ_FULL_LIST);
$MyReqMSGPerEmail = $MyDB->prepare(REQ_MSG_PER_EMAIL);
$MyReqNbMsgPerMonth = $MyDB->prepare(REQ_NB_MSG_PER_MONTH);
$MyReqEtatMailList = $MyDB->prepare(REQ_ETAT_MAIL_LIST);
$MyReqUpdateMailEtat = $MyDB->prepare(REQ_UPDATE_MAIL_ETAT);
$MyReqEtatMailPerId = $MyDB->prepare(REQ_ETAT_MAIL_PER_ID);
(string) $Reponse = "";
(string) $Answer = "";
while ("0" != $Answer) {
    print "Que voulez-vous faire ?" . PHP_EOL;
    print "1 > Afficher la liste complète des messages" . PHP_EOL;
    print "2 > Afficher la liste complète des messages par email" . PHP_EOL;
    print "3 > Afficher le nombre de message par mois" . PHP_EOL;
    print "4 > Afficher les messages par état(à traiter,à relancer, en attente de réponse, RDV pris, sans suite)" . PHP_EOL;
    print "5 > Mettre à jour un statut de mail par ID" . PHP_EOL;
    print "0 > Arrêter le programme" . PHP_EOL;
    print "Votre choix : ";    $Answer = trim(fgets(STDIN));
    switch ($Answer) {
        case "1":
            RequestManager($MyReqFullList, "La liste complète des messages :", "Il n'y a aucun message.");
            break;


        case "2":
            print "OBLIGATOIRE - Saisissez l'ID de l'adresse mail souhaitée pour avoir la liste des emails correspondant à l'ID: ";
            (string) $EmailID = trim(fgets(STDIN));
            $MyReqParams = [":ValEmailID" => $EmailID];
            RequestManager($MyReqMSGPerEmail, "La liste complète des messages par adresse ID email :", "Il n'y a aucun messages avec cet email.", $MyReqParams);
            break;



        case "3":
            print "Saisissez le mois pour visualiser le nombre de messages : ";
            (int) $Mois = intval(trim(fgets(STDIN)));
            $MyReqParams = [":Mois" => $Mois,];
            RequestManager($MyReqNbMsgPerMonth, "Le nombre de messages par mois est:", "Il n'y a aucun message.", $MyReqParams);
            break;


        case "4":            
            while ("0" != $Reponse) { 
                print "Que voulez-vous faire ?" . PHP_EOL;
                print "1 > Récupérer les messages à traiter?" . PHP_EOL;
                print "2 > Récupérer les messages à relancer?" . PHP_EOL;
                print "3 > Récupérer les messages en attente de réponse?" . PHP_EOL;
                print "4 > Récupérer les messages des RDV pris?" . PHP_EOL;
                print "5 > Récupérer les messages sans suite?" . PHP_EOL;
                print "6 > Récupérer les messages traités?" . PHP_EOL;
                print "0 > Revenir sur le menu précédent" . PHP_EOL;
                print "Votre choix : ";
                
                $Reponse = trim(fgets(STDIN));

                    switch ($Reponse) {
                        case "1":
                            $MyReqParams = [
                            ":Etat" =>"à traiter",
                            ];
                            
                            RequestManager($MyReqEtatMailList, "La liste complète des messages à traiter:", "Il n'y a aucun message.", $MyReqParams);
                            break;

                        case "2":
                            $MyReqParams = [
                            ":Etat" => "à relancer",
                            ];
                            RequestManager($MyReqEtatMailList, "La liste complète des messages à relancer:", "Il n'y a aucun message.", $MyReqParams);
                            break;
                        case "3":
                            $MyReqParams = [
                            ":Etat" => "en attente de réponse",
                            ];
                            RequestManager($MyReqEtatMailList, "La liste complète des messages en attente de réponse:", "Il n'y a aucun message.", $MyReqParams);
                            break;
                        case "4":
                            $MyReqParams = [
                            ":Etat" => "RDV pris",
                            ];
                            RequestManager($MyReqEtatMailList, "La liste complète des messages des RDV pris", "Il n'y a aucun message.", $MyReqParams);
                            break;
                        case "5":
                            $MyReqParams = [
                            ":Etat" => "sans suite"
                            ];
                            RequestManager($MyReqEtatMailList, "La liste complète des messages sans suite", "Il n'y a aucun message.", $MyReqParams);
                            break;
                        case "6":
                            $MyReqParams = [
                            ":Etat" => "traité"
                            ];
                            RequestManager($MyReqEtatMailList, "La liste complète des messages traités", "Il n'y a aucun message.", $MyReqParams);
                            break;
                        case "0":
                            break; 
                        default:
                            print PHP_EOL . "Je ne comprends votre réponse, merci de saisir à nouveau votre choix." . PHP_EOL . PHP_EOL;
                            break;
                    }
            }
        break;

        case '5':
            // transaction= chaine plusieurs requètes les une aux autres pour faire quelque chose dessus
            if (!($MyDB->inTransaction())) {
                try {
                    $MyDB->beginTransaction();
                    print PHP_EOL . "Cette transaction va faire 2 choses :";
                    print PHP_EOL . "-Copier les informations de l'état du mail". PHP_EOL;
                    print  "-Changer l'état du traitement du message" . PHP_EOL;

                    // Demander l'ID du message à traiter
                    print PHP_EOL . "Saisissez l'ID du message à traiter : ";
                    (string) $EmailID = trim(fgets(STDIN));
                    $MyReqParams = [
                        ":ValEmailID" => $EmailID
                    ];

                    // Récupérer l'état de la requète
                    (bool) $ReqResult = $MyReqEtatMailPerId->execute($MyReqParams);
                    
                    
                    if ($ReqResult) {
                        //Si il y a un résultat alors on copie dans le tableau dataline
                        (array) $DataLine = $MyReqEtatMailPerId->fetch(PDO::FETCH_ASSOC);
                        
                        //on fait
                        while (("0" != $Reponse)) {
                            print "A quel état voulez-vous mettre le message?" . PHP_EOL;
                            print "1 >  à  traiter" . PHP_EOL;
                            print "2 >  à relancer" . PHP_EOL;
                            print "3 >  en attente de réponse" . PHP_EOL;
                            print "4 >  RDV pris" . PHP_EOL;
                            print "5 >  sans suite" . PHP_EOL;
                            print "0 > arrêter le programme" . PHP_EOL;
                            print "Votre choix : ";
                            // strtoupper =>Renvoie une chaîne en majuscules
                            (string)$Reponse = trim(fgets(STDIN));
                            
                            switch ($Reponse) {
                                case "1":
                                    $MyReqParams = [
                                    ":NewEtat" => 'à  traiter',
                                    ":ValEmailID" => $EmailID
                                    ];
                                    
                                    RequestManager($MyReqUpdateMailEtat, "Le message avec l'ID".$EmailID." a bien été mis à jour.", "Il n'y a aucun message.", $MyReqParams,false);
                                    break;
                                case "2":
                                    $MyReqParams = [
                                    ":NewEtat" => "à relancer",
                                    ":ValEmailID" => $EmailID
                                    ];
                                    
                                    RequestManager($MyReqUpdateMailEtat, "Le message avec l'ID".$EmailID." a bien été mis à jour.", "Il n'y a aucun message.", $MyReqParams,false);
                                    break;
                                case "3":
                                    $MyReqParams = [
                                    ":NewEtat" => "en attente de réponse",
                                    ":ValEmailID" => $EmailID
                                    ];
                                    
                                    RequestManager($MyReqUpdateMailEtat, "Le message avec l'ID ".$EmailID." a bien été mis à jour.", "Il n'y a aucun message.", $MyReqParams,false);
                                    break;
                                case "4":
                                    $MyReqParams = [
                                    ":NewEtat" => "RDV pris",
                                    ":ValEmailID" => $EmailID
                                    ];
                                    
                                    RequestManager($MyReqUpdateMailEtat, "Le message avec l'ID ".$EmailID." a bien été mis à jour.", "Il n'y a aucun message.", $MyReqParams,false);
                                    break;
                                case "5":
                                    $MyReqParams = [
                                    ":NewEtat" => "sans suite",
                                    ":ValEmailID" => $EmailID
                                    ];
                                    
                                    RequestManager($MyReqUpdateMailEtat, "Le message avec l'ID ".$EmailID." a bien été mis à jour.", "Il n'y a aucun message.", $MyReqParams,false);
                                    break;
                                case "0":
                                    print PHP_EOL . "Au revoir." . PHP_EOL;
                                    break;
                                default:
                                    print PHP_EOL . "Je ne comprends votre réponse, merci de saisir à nouveau votre choix." . PHP_EOL . PHP_EOL;
                                    break;
                            }


                            /*   (array) $MyParams = [];
                               boolval= Returns the bool value of value.
                               $DataLineElem = boolval(count($DataLine));

                                hile ($DataLineElem !== false) {
                                   if (("etat" != key($DataLine))) {
                                       $MyParams[":" . key($DataLine)] = $DataLine[key($DataLine)];
                                    }
                                    $DataLineElem = next($DataLine);
                                }

                                // L'execute avec les nouveaux paramÃƒÂ¨tres
                                $MyReqUpdateMailEtat->execute($MyParams);

                                // Demander la confirmation de l'ÃƒÂ©change d'ID
                                $Answer = "";*/
                                while (("O" != $Answer) && ("N" != $Answer)) {
                                    print PHP_EOL . "Confirmez-vous l'échange d'ID ? (O/N) ";
                                    // strtoupper Ã¢â‚¬â€� Renvoie une chaÃ®ne en majuscules
                                    $Answer = strtoupper(trim(fgets(STDIN)));
                                    switch ($Answer) {
                                        case "O":
                                            print PHP_EOL . "Confirmation de la mise à jour." . PHP_EOL . PHP_EOL;
                                            $MyDB->commit();
                                            break;
                                        case "N":
                                            print PHP_EOL . "Annulation de la mise à jour." . PHP_EOL . PHP_EOL;
                                            $MyDB->rollBack();
                                            break;
                                        default:
                                            print PHP_EOL . "Je n'ai pas compris votre réponse." . PHP_EOL . PHP_EOL;
                                            break;
                                    }
                                }
                            
                        }
                    }
                } catch (Exception $ExceptionRaised) {
                    $MyDB->rollBack();
                    print_r($MyDB->errorInfo());
                    // die= Affiche un message et termine le script courant (equivalent ÃƒÂ  exit)
                    die("Error: " . $ExceptionRaised->getMessage());
                }
            } else {
                print 'Impossible de commencer la transaction. Une autre transaction est déjà en cours.' . PHP_EOL . PHP_EOL;
            }
        break;
        case "0":
            print PHP_EOL . "Au revoir." . PHP_EOL;
            break;
        default:
            print PHP_EOL . "Je ne comprends votre réponse, merci de saisir à nouveau votre choix." . PHP_EOL . PHP_EOL;
            break;
    }
}
